import React from 'react';
import Question from './Question';
import './styles.css';
import questions from './questions';
import emailSender from './emailSender';

class Quiz extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: questions,
      currentQuestionIndex: 0,
      userAnswers: [],
      showResult: false,
      quizStarted: false,
      timer: 60,
      playerName: '',
      timerStarted: false, // Variable de estado para controlar si el temporizador se ha iniciado
    };
    this.timerInterval = null;
  }

  componentDidMount() {
    // El temporizador se iniciará después de que el usuario presione "Empezar Quiz"
  }

  componentWillUnmount() {
    this.stopTimer();
  }

  startTimer() {
    this.setState({ timerStarted: true }, () => {
      this.timerInterval = setInterval(() => {
        this.setState((prevState) => ({
          timer: prevState.timer - 1,
        }), () => {
          if (this.state.timer === 0) {
            this.goToNextQuestion();
          }
        });
      }, 1000);
    });
  }

  stopTimer() {
    clearInterval(this.timerInterval);
  }

  goToNextQuestion() {
    const { currentQuestionIndex, questions } = this.state;
    if (currentQuestionIndex < questions.length - 1) {
      this.setState(
        (prevState) => ({
          currentQuestionIndex: prevState.currentQuestionIndex + 1,
          userAnswers: [...prevState.userAnswers, undefined],
          timer: 60,
        }),
        () => {
          this.startTimer();
        }
      );
    } else {
      this.setState({ showResult: true });
    }
  }

  handleAnswerSelect = (selectedAnswerIndex) => {
    const { currentQuestionIndex, userAnswers, quizStarted } = this.state;

    if (!quizStarted) {
      this.setState({ quizStarted: true });
      this.startTimer(); // Iniciar el temporizador cuando el usuario presione "Empezar Quiz"
      return;
    }

    this.stopTimer();

    const updatedUserAnswers = [...userAnswers];
    updatedUserAnswers[currentQuestionIndex] = selectedAnswerIndex;

    this.setState({ userAnswers: updatedUserAnswers });

    this.goToNextQuestion();
  };

  handlePlayerNameChange = (event) => {
    this.setState({ playerName: event.target.value });
  };

  renderResult() {
    const { userAnswers, questions, playerName } = this.state;
    let correctAnswers = 0;

    for (let i = 0; i < questions.length; i++) {
      const question = questions[i];
      const userAnswerIndex = userAnswers[i];

      if (userAnswerIndex === question.correctAnswer) {
        correctAnswers++;
      }
    }

    const congratsMessage = correctAnswers > 5 ? '¡Enhorabuena!' : '¡Sigue practicando!';

    // Enviar los resultados a través de la notificación de Slack
    emailSender.sendNotification({
      playerName: playerName,
      correctAnswers: correctAnswers,
    });

    return (
      <div className="result-container">
        <h2>Quiz completado</h2>
        <p>Respuestas correctas: {correctAnswers} de {questions.length}</p>
        <p>Nombre: {playerName}</p>
        <p>{congratsMessage}</p>
        <button onClick={this.resetQuiz} className="reset-button">
          Reiniciar Quiz
        </button>
      </div>
    );
  }

  resetQuiz = () => {
    this.setState(
      {
        currentQuestionIndex: 0,
        userAnswers: [],
        showResult: false,
        quizStarted: false,
        timer: 60,
        playerName: '',
        timerStarted: false, // Reiniciar el estado del temporizador
      },
      () => {
        this.stopTimer();
      }
    );
  };

  renderTimer() {
    const { timer, timerStarted } = this.state;

    // Mostrar el temporizador solo si el temporizador se ha iniciado
    if (!timerStarted) {
      return null;
    }

    const minutes = Math.floor(timer / 60).toString().padStart(2, '0');
    const seconds = (timer % 60).toString().padStart(2, '0');

    return <div className="timer">{minutes}:{seconds}</div>;
  }

  render() {
    const { questions, currentQuestionIndex, userAnswers, showResult, quizStarted, playerName } = this.state;
    const currentQuestion = questions[currentQuestionIndex];

    return (
      <div className="quiz-wrapper">
        <div className="quiz-container">
          <h1 className="quiz-title">Quiz sobre Docker, Docker-compose y GitLab</h1>
          {showResult ? (
            this.renderResult()
          ) : (
            <div>
              {quizStarted ? (
                <div>
                  <Question
                    question={currentQuestion.question}
                    answers={currentQuestion.answers}
                    userAnswer={userAnswers[currentQuestionIndex]}
                    onAnswerSelect={this.handleAnswerSelect}
                  />
                  {this.renderTimer()}
                </div>
              ) : (
                <div>
                  <input
                    type="text"
                    placeholder="Nombre"
                    value={playerName}
                    onChange={this.handlePlayerNameChange}
                  />
                  <button onClick={() => this.handleAnswerSelect()} className="start-button">
                    Empezar Quiz
                  </button>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Quiz;
