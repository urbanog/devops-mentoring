import axios from 'axios';

const sendNotification = (result) => {
  const token = 'T05A4QENPV3/B05AXEJGXNU/ZaPoGQ2S0tTkA0sEwUlZHhyI'
  const webhookUrl = `/services/${token}`; // Reemplaza 'TOKEN' con tu webhook URL de Slack
  const message = `¡Nuevo resultado de quiz!\nNombre: ${result.playerName}\nRespuestas correctas: ${result.correctAnswers}`;

  return axios.post(webhookUrl, { text: message });
};

const emailSender = {
  sendNotification: sendNotification,
};

export default emailSender;
