FROM node:16-alpine as build
RUN apk update
WORKDIR /usr/src/app
ADD ./ ./
RUN npm install --omit-dev
RUN npm run build

# this is a comment
EXPOSE 3000
CMD ["npm", "run", "build"]

FROM nginx:1.24.0-alpine
COPY --from=build /usr/src/app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]